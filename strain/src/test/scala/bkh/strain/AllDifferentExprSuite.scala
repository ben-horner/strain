package bkh.strain

import bkh.strain.domain.ExplicitDomain
import bkh.strain.expr.{AllDifferentExpr, ExplicitVar}
import bkh.strain.system.{DomainStore, ReductionResult, SymbolicSystem}
import org.junit.runner.RunWith
import org.scalatest.funsuite.AnyFunSuite
import org.scalatestplus.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class AllDifferentExprSuite extends AnyFunSuite {

  test("AllDifferentExpr over disjoint Exprs should be happy") {
    val ev1 = ExplicitVar[Int](ExplicitDomain[Int](Set(1)))
    val ev2 = ExplicitVar[Int](ExplicitDomain[Int](Set(2)))
    val ad = AllDifferentExpr(Seq(ev1, ev2), Set(true, false))

    var ss = SymbolicSystem()
    ss = ss.add(ev1, "ev1")
    ss = ss.add(ev2, "ev2")
    ss = ss.add(ad, "ad")
    val ds = DomainStore(ss)

    assert(ad.subExprs.size == 2)
    assert(ad.initialDomain == ExplicitDomain.AllBools)
    assert(ad.domain(ds) == ExplicitDomain.AllBools)
    assert(ad.computeDomain(ds) == ExplicitDomain.TrueOnly) // TODO: produce scenarios for FalseOnly and AllBools
    val reductions = ad.generateReductions(ds)
    assert(reductions.size == 1)
    assert(reductions.head.exprToReduce == ad)
    val (ds2, res) = reductions.head.reduce(ds)
    assert(res == ReductionResult.ReducedToBound)
    assert(ad.domain(ds2) == ExplicitDomain.TrueOnly)
    assert(ev1.domain(ds2) == ev1.domain(ds))
    assert(ev2.domain(ds2) == ev2.domain(ds))

    assert(ad.remove(ExplicitDomain[Boolean](Set()), ds)._1.domain(ad.id) == ExplicitDomain.AllBools)
    assert(ad.remove(ExplicitDomain[Boolean](Set()), ds)._2 == ReductionResult.NoReduction)

    assert(ad.remove(ExplicitDomain.FalseOnly, ds)._1.domain(ad.id) == ExplicitDomain.TrueOnly)
    assert(ad.remove(ExplicitDomain.FalseOnly, ds)._2 == ReductionResult.ReducedToBound)

    assert(ad.remove(ExplicitDomain.TrueOnly, ds)._1.domain(ad.id) == ExplicitDomain.FalseOnly)
    assert(ad.remove(ExplicitDomain.TrueOnly, ds)._2 == ReductionResult.ReducedToBound)

    assert(ad.remove(ExplicitDomain.AllBools, ds)._1.domain(ad.id) == ExplicitDomain[Boolean](Set()))
    assert(ad.remove(ExplicitDomain.AllBools, ds)._2 == ReductionResult.ReducedToEmpty)

    assert(ad.restrictTo(ExplicitDomain[Boolean](Set()), ds)._1.domain(ad.id) == ExplicitDomain[Boolean](Set()))
    assert(ad.restrictTo(ExplicitDomain[Boolean](Set()), ds)._2 == ReductionResult.ReducedToEmpty)

    assert(ad.restrictTo(ExplicitDomain.FalseOnly, ds)._1.domain(ad.id) == ExplicitDomain.FalseOnly)
    assert(ad.restrictTo(ExplicitDomain.FalseOnly, ds)._2 == ReductionResult.ReducedToBound)

    assert(ad.restrictTo(ExplicitDomain.TrueOnly, ds)._1.domain(ad.id) == ExplicitDomain.TrueOnly)
    assert(ad.restrictTo(ExplicitDomain.TrueOnly, ds)._2 == ReductionResult.ReducedToBound)

    assert(ad.restrictTo(ExplicitDomain.AllBools, ds)._1.domain(ad.id) == ExplicitDomain.AllBools)
    assert(ad.restrictTo(ExplicitDomain.AllBools, ds)._2 == ReductionResult.NoReduction)
  }

  test("AllDifferentExpr.computeDomain() should be happy") {
    val ev1 = ExplicitVar[Int](ExplicitDomain[Int](Set(1)))
    val ev2 = ExplicitVar[Int](ExplicitDomain[Int](Set(1,2)))
    val ad = AllDifferentExpr(Seq(ev1, ev2), ExplicitDomain.TrueOnly)

    var ss = SymbolicSystem()
    ss = ss.add(ev1, "ev1")
    ss = ss.add(ev2, "ev2")
    ss = ss.add(ad, "ad")
    val ds = DomainStore(ss)

    val reductions = ad.generateReductions(ds)
    assert(reductions.size == 1)

    val (ds2, result) = reductions.head.reduce(ds)
    assert(ev2.domain(ds) == ExplicitDomain(Set(1,2)))
    assert(ev2.domain(ds2) == ExplicitDomain(Set(2)))
    assert(result == ReductionResult.ReducedToBound)
  }

}
