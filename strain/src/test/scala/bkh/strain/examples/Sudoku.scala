package bkh.strain.examples

import bkh.layout.{ColElement, Element, RowElement}
import bkh.strain._
import bkh.strain.domain.ExplicitDomain
import bkh.strain.expr.{AllDifferentExpr, ExplicitVar, OneOfExpr}
import bkh.strain.system.ReductionChooser.SmallestBisectingChooser
import bkh.strain.system.{DomainStore, SymbolicSystem}

import scala.io.Source

object Sudoku {

  def main(args: Array[String]): Unit = {
    val s: Sudoku = Sudoku.load("/Users/ben/git/robocode/lib/src/test/resources/sudoku")
    println("loaded sudoku from file:")
    println(s.toString())

    val constraintProgram: SymbolicSystem = s.toConstraintProgram()

    val reductionChooser = SmallestBisectingChooser

    var searchStack: Seq[SearchNode] = Solver.initialSearchState(constraintProgram)
    var i = 0
    while (!searchStack.isEmpty && !searchStack.head.nextState.isBound) {
      println(s"after step $i (stack is ${searchStack.size} deep):")
      println(s"\t${Sudoku.numCellsBound(constraintProgram, searchStack.head.nextState)} cells bound")
      println(s"\t${Sudoku.numVarsBoundUnbound(searchStack.head.nextState)} Exprs bound and unbound")
      //      println(Sudoku.constrainedStateToString(constraintProgram, searchStack.head.nextState))
      i = i + 1
      searchStack = Solver.step(searchStack, reductionChooser, constraintProgram)
    }

    val solution = Sudoku.boundCellsToSudoku(constraintProgram, searchStack.head.nextState)
    require(solution.isCompleted && solution.isConsistent)
    println(solution.toString)

    // define problem symbolically
    // * variables (with initial domains)
    // * more complex expressions (whose initial domains can be computed)
    // * all expressions need unique identifiers

    // a search node will be a pair of
    // * choice (an "unnecessary" reduction, exploring the tree of choices should lead to discovery of infeasiblity, or else a feasible bound solution)
    // * search state
    // a search will be a sequence of choices which decreased domains
    // during a search, a choice may lead to infeasibility, if that's the case, we can backtrack by making the opposite choice
    // during a search, we may find that the original state of the problem is infeasible
    // during a search, we may find a feasible solution where all domains are bound

    // construct the initial search state
    // * this is a mapping from expression identifiers to initial domains (a domain store)
    // * propagate any domain reductions through expressions
    // while the latest domain store is not infeasible and not bound
    // * choose an expression whose domain to reduce (smallest?)
    // * choose a reduction for that domain (bisect?)
    // * propagate any domain reductions through expressions
    // *

    // how do backtrack based reductions fit into the search?

    // * choice
    // * infeasible next choices
    // * state including propagation from choice and (opposite of) infeasible choices



    println("Hello World!")
  }

  def load(file: String): Sudoku = {
    val source = Source.fromFile(file)
    val cellStrings = source.
      getLines().toSeq.
      filterNot(l => l.contains('-') || l.trim().isEmpty).
      map(_.replaceAll("\\|\\|", "|")).
      map(_.split("\\|").toSeq.drop(1))
    source.close()
    require(cellStrings.size == 9, s"file had wrong number of sudoku lines (${cellStrings.size}")
    require(cellStrings.forall(_.size == 9), s"some sudoku line had wrong number of cells (${cellStrings.map(_.size).mkString(",")})")
    val cells: Map[(Int, Int), Int] = (for {
      row <- 0 until 9
      col <- 0 until 9
      if cellStrings(row)(col).trim().nonEmpty
    } yield {
      (row, col) -> cellStrings(row)(col).trim.toInt
    }).toMap
    val result = new Sudoku(cells)
    require(result.isConsistent)
    result
  }

  def numCellsBound(system: SymbolicSystem, state: DomainStore): Int = {
    (0 until 9).flatMap { row =>
      (0 until 9).map { col =>
        val cellName = s"cell$row,$col"
        if (system.exprWithName(cellName).domain(state).isBound) {
          1
        } else {
          0
        }
      }
    }.sum
  }

  def numVarsBoundUnbound(state: DomainStore): (Int, Int) = {
    val (bound, unbound) = state.idToDomain.values.partition(_.isBound)
    (bound.size, unbound.size)
  }

  def boundCellsToSudoku(system: SymbolicSystem, state: DomainStore): Sudoku = {
    val cellToName: Map[(Int, Int), String] = (for {
      row <- 0 until 9
      col <- 0 until 9
    } yield {
      (row, col) -> s"cell$row,$col"
    }).toMap
    val boundCellToValue: Map[(Int, Int), Int] = cellToName.flatMap{ case (cell, name) =>
      if (system.exprWithName(name).domain(state).isBound) {
        val boundValue = system.exprWithName(name).asInstanceOf[ExplicitVar[Int]].domain(state).values.head
        Some(cell -> boundValue)
      } else {
        None
      }
    }
    Sudoku(boundCellToValue)
  }

  def constrainedStateToString(system: SymbolicSystem, state: DomainStore): String = {
    def layoutCell(explicitDomain: ExplicitDomain[Int]): Element = {
      val col = (0 until 3).map { rowIndex =>
        val row = (0 until 3).map { colIndex =>
          val i = 3 * rowIndex + colIndex + 1
          if (explicitDomain.values.contains(i)) {
            Element(s"$i")
          } else {
            Element(" ")
          }
        }
        RowElement(row).divided(' ').widen(7)
      }
      ColElement(col)
    }

    val boxCol = (0 until 3).map { boxRowIndex =>
      val boxRow = (0 until 3).map { boxColIndex =>
        val subRow = (0 until 3).map { subColIndex =>
          val col = 3 * boxColIndex + subColIndex
          val subCol = (0 until 3).map { subRowIndex =>
            val row = 3 * boxRowIndex + subRowIndex
            val cellName = s"cell$row,$col"
            layoutCell(system.exprWithName(cellName).domain(state).asInstanceOf[ExplicitDomain[Int]])
          }
          ColElement(subCol).divided()
        }
        RowElement(subRow).divided().boxed()
      }
      Element.row(boxRow)
    }
    Element.col(boxCol).render.mkString("\n")
  }

}

case class Sudoku(cells: Map[(Int, Int), Int]) {

  def isConsistent: Boolean = {
    var rows: Map[Int, Seq[Int]] = Map()
    var cols: Map[Int, Seq[Int]] = Map()
    var boxes: Map[(Int, Int), Seq[Int]] = Map()
    for {
      row <- 0 until 9
      col <- 0 until 9
      boxKey = (row / 3, col / 3)
      valueOpt = get(row, col)
      if valueOpt.isDefined
      value = valueOpt.get
    } {
      rows = rows + (row -> (rows.getOrElse(row, Seq()) :+ value))
      cols = cols + (col -> (cols.getOrElse(col, Seq()) :+ value))
      boxes = boxes + (boxKey -> (boxes.getOrElse(boxKey, Seq()) :+ value))
    }
    rows.values.forall(row => row.size == row.toSet.size) &&
      cols.values.forall(col => col.size == col.toSet.size) &&
      boxes.values.forall(box => box.size == box.toSet.size)
  }

  def isCompleted: Boolean = {
    cells.size == 81
  }

  def get(row: Int, col: Int): Option[Int] = {
    cells.get((row, col))
  }

  def isDefined(row: Int, col: Int): Boolean = {
    get(row, col).isDefined
  }

  override def toString: String = {
    val boxCol = (0 until 3).map { boxRowIndex =>
      val boxRow = (0 until 3).map { boxColIndex =>
        val subRow = (0 until 3).map { subColIndex =>
          val col = 3 * boxColIndex + subColIndex
          val subCol = (0 until 3).map { subRowIndex =>
            val row = 3 * boxRowIndex + subRowIndex
            get(row, col).map(i => Element(i.toString)).getOrElse(Element(" ")).widen(3)
          }
          Element.col(subCol)
        }
        RowElement(subRow).divided().boxed()
      }
      Element.row(boxRow)
    }
    Element.col(boxCol).render.mkString("\n")
  }

  def toConstraintProgram(): SymbolicSystem = {
    var rowToCells = Map[Int, Seq[ExplicitVar[Int]]]()
    var colToCells = Map[Int, Seq[ExplicitVar[Int]]]()
    var boxToCells = Map[(Int, Int), Seq[ExplicitVar[Int]]]()
    var result: SymbolicSystem = SymbolicSystem()
    for {
      row <- 0 until 9
      col <- 0 until 9
      boxKey = (row / 3, col / 3)
    } {
      val cell: ExplicitVar[Int] = get(row, col). // get the Option[Int] for the cell
        map(value => ExplicitVar[Int](Set(value))). // if the cell contained Some[Int], create a single value Expr[Int]
        getOrElse(ExplicitVar[Int]((1 to 9).toSet)) // otherwise create an Expr[Int] containing all 9 values
      result = result.add(cell, s"cell$row,$col")
      rowToCells = rowToCells + (row -> (rowToCells.getOrElse(row, Seq()) :+ cell))
      colToCells = colToCells + (col -> (colToCells.getOrElse(col, Seq()) :+ cell))
      boxToCells = boxToCells + (boxKey -> (boxToCells.getOrElse(boxKey, Seq()) :+ cell))
    }
    val distinctCellGroups =
      rowToCells.map { case (i, cells) => s"row$i" -> cells } ++
        colToCells.map { case (i, cells) => s"col$i" -> cells } ++
        boxToCells.map { case ((r, c), cells) => s"box$r$c" -> cells }
    // no two cells in a distinct cell group can have the same value
    distinctCellGroups.foreach { case (name, cells) =>
      require(cells.toSet.size == 9)
      (1 to 9).foreach { i =>
        // one-of each distinct cell group must be each number (i.e. one cell of each row must be a "1")
        result = result.add(OneOfExpr(cells, initialDomain = ExplicitDomain(Set(i))), s"${name}_has_$i")
      }
      result = result.add(AllDifferentExpr(cells, Set(true)), s"${name}_all_diff")
    }
    result
  }

}