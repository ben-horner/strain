package bkh.strain.examples

import bkh.strain.{SearchNode, Solver}
import bkh.strain.expr.ExplicitVar
import bkh.strain.system.ReductionChooser.SmallestBisectingChooser
import bkh.strain.system.{Expr, SymbolicSystem}

//object SendMoreMoney {
//
//  val digitDomain = (0 until 9).toSet
//  val s = ExplicitVar[Long](digitDomain)
//  val e = ExplicitVar[Long](digitDomain)
//  val n = ExplicitVar[Long](digitDomain)
//  val d = ExplicitVar[Long](digitDomain)
//  val m = ExplicitVar[Long](digitDomain)
//  val o = ExplicitVar[Long](digitDomain)
//  val r = ExplicitVar[Long](digitDomain)
//  val y = ExplicitVar[Long](digitDomain)
//
//  def word(letters: Seq[ExplicitVar[Long]]): Expr = {
//    val coeffs: Seq[Long] = ((letters.size-1) to 0 by -1).map(Math.pow(10.0, _).round)
//    require(coeffs.size == letters.size)
//    val terms: Seq[] = letters.zip(coeffs).map{ case (letter, coeff) => ExplicitVar[Long](Set(coeff)) * letter }
//    terms.reduce(_ + _)
//  }
//
//  val constraint = (word(Seq(s, e, n, d)) + word(Seq(m, o, r, e)) == word(Seq(m, o, n, e, y))).true()
//
//  def main(args: Seq[String]): Unit = {
//    val constraintProgram = SymbolicSystem()
//      .add(s, "s")
//      .add(e, "e")
//      .add(n, "n")
//      .add(d, "d")
//      .add(m, "m")
//      .add(o, "o")
//      .add(r, "r")
//      .add(y, "y")
//      .add(constraint)
//    val reductionChooser = SmallestBisectingChooser
//
//    var searchStack: Seq[SearchNode] = Solver.initialSearchState(constraintProgram)
//    var i = 0
//    while (!searchStack.isEmpty && !searchStack.head.nextState.isBound) {
//      searchStack = Solver.step(searchStack, reductionChooser, constraintProgram)
//    }
//
//  }

}
