package bkh.strain

import bkh.strain.domain.ExplicitDomain
import bkh.strain.system.ReductionResult
import org.junit.runner.RunWith
import org.scalatest.funsuite.AnyFunSuite
import org.scalatestplus.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class ExplicitDomainSuite extends AnyFunSuite {

  test("Empty ExplicitDomain should be happy") {
    val ed = ExplicitDomain[Int](Set[Int]())
    assert(ed.isEmpty)
    assert(!ed.isBound)
    val (removed, removeRes) = ed.remove(ed)
    assert(removeRes == ReductionResult.NoReduction)
    assert(removed == ed)
    val (restricted, restrictRes) = ed.restrictTo(ed)
    assert(restrictRes == ReductionResult.NoReduction)
    assert(restricted == ed)

    val half = ed.bisect()
    assert(half == ed)
  }

  test("Singleton ExplicitDomain should be happy") {
    val ed = ExplicitDomain[Int](Set(1))
    assert(!ed.isEmpty)
    assert(ed.isBound)
    val (removed, removeRes) = ed.remove(ed)
    assert(removeRes == ReductionResult.ReducedToEmpty)
    assert(removed.isEmpty)
    val (restricted, restrictRes) = ed.restrictTo(ed)
    assert(restrictRes == ReductionResult.NoReduction)
    assert(restricted == ed)

    val half = ed.bisect()
    assert(half == ed)
  }

  test("Multi-valued ExplicitDomain should be happy") {
    val ed = ExplicitDomain[Int](Set(1,2))
    assert(!ed.isEmpty)
    assert(!ed.isBound)
    val (removed, removeRes) = ed.remove(ed)
    assert(removeRes == ReductionResult.ReducedToEmpty)
    assert(removed.isEmpty)
    val (restricted, restrictRes) = ed.restrictTo(ed)
    assert(restrictRes == ReductionResult.NoReduction)
    assert(restricted == ed)

    val half = ed.bisect()
    assert(half.isBound)
  }

  test("ExplicitDomain.remove() should be happy") {
    val ed3Small = ExplicitDomain[Int](Set(1,2,3))
    val ed3Mid = ExplicitDomain[Int](Set(2,3,4))
    val ed3Big = ExplicitDomain[Int](Set(4,5,6))

    val (sm, smRes) = ed3Small.remove(ed3Mid)
    assert(smRes == ReductionResult.ReducedToBound)
    assert(sm.isBound)
    val (sb, sbRes) = ed3Small.remove(ed3Big)
    assert(sbRes == ReductionResult.NoReduction)
    assert(ed3Small == sb)

    val (ms, msRes) = ed3Mid.remove(ed3Small)
    assert(msRes == ReductionResult.ReducedToBound)
    assert(ms.isBound)
    val (mb, mbRes) = ed3Mid.remove(ed3Big)
    assert(mbRes == ReductionResult.Reduced)
    assert(mb.values.size == 2)

    val (bs, bsRes) = ed3Big.remove(ed3Small)
    assert(bsRes == ReductionResult.NoReduction)
    assert(bs.values.size == 3)
    val (bm, bmRes) = ed3Big.remove(ed3Mid)
    assert(bmRes == ReductionResult.Reduced)
    assert(bm.values.size == 2)
  }

  test("ExplicitDomain.restrictTo() should be happy") {
    val ed3Small = ExplicitDomain[Int](Set(1,2,3))
    val ed3Mid = ExplicitDomain[Int](Set(2,3,4))
    val ed3Big = ExplicitDomain[Int](Set(4,5,6))

    val (sm, smRes) = ed3Small.restrictTo(ed3Mid)
    assert(smRes == ReductionResult.Reduced)
    assert(sm.values.size == 2)
    val (sb, sbRes) = ed3Small.restrictTo(ed3Big)
    assert(sbRes == ReductionResult.ReducedToEmpty)
    assert(sb.isEmpty)

    val (ms, msRes) = ed3Mid.restrictTo(ed3Small)
    assert(msRes == ReductionResult.Reduced)
    assert(ms.values.size == 2)
    val (mb, mbRes) = ed3Mid.restrictTo(ed3Big)
    assert(mbRes == ReductionResult.ReducedToBound)
    assert(mb.isBound)

    val (bs, bsRes) = ed3Big.restrictTo(ed3Small)
    assert(bsRes == ReductionResult.ReducedToEmpty)
    assert(bs.isEmpty)
    val (bm, bmRes) = ed3Big.restrictTo(ed3Mid)
    assert(bmRes == ReductionResult.ReducedToBound)
    assert(bm.isBound)
  }

  test("ExplicitDomain.bisect() should be happy") {
    val ed3 = ExplicitDomain[Int](Set(1,2,3))
    assert(ed3.bisect().values.size == 2)
    val ed4 = ExplicitDomain[Int](Set(4,5,6,7))
    assert(ed4.bisect().values.size == 2)
    val ed5 = ExplicitDomain[Int](Set(8,9,10,11,12))
    assert(ed5.bisect().values.size == 3)
  }

}
