package bkh.strain

import bkh.strain.system.{DomainReduction, DomainStore, Expr, ReductionChooser, ReductionResult, RestrictToReduction, SymbolicSystem}
import org.apache.log4j.Logger

//trait Solver {
//  def system: SymbolicSystem
//  def reductionChooser: ReductionChooser
//
//  def initialState: DomainStore
//  def reducedInitialState: DomainStore = reduce(initialState)
//  def initialChoice: DomainReduction = reductionChooser.chooseReduction(reducedInitialState)
//  def initialSearch: Seq[SearchNode] = Seq(SearchNode(initialState, initialChoice, Seq()))
//
//  // if the problem is solved, make no change
//  // in all other cases, produce a new most-recent node for the search (might backtrack, might just proceed a step)
//  // if the latest node of the search is unbound and potentially feasible, take another step toward binding (reduce a domain)
//  // if the latest node is infeasible, backtrack, and add the infeasible choice at the appropriate point (which should enforce the opposite choice)
//  def step(searchState: Seq[SearchNode]): Seq[SearchNode] = {
//    val SearchNode(state, exploringChoice, infeasibleChoices) = searchState.last
//
//    while (searchState.last.)
//  }
//
//  def solution: DomainStore
//
//}

case class SearchNode(prevState: DomainStore, choice: DomainReduction, triedOpposite: Boolean, nextState: DomainStore)

object Solver {
  val logger = Logger.getLogger(Solver.getClass)

  // propagate can be refined, we ca track which expressions may have reductions, based on what reductions happened so far
  // (and exclude expressions which can not have any reductions, to save that work)
  def propagate(state: DomainStore, system: SymbolicSystem): (DomainStore, Set[ReductionResult]) = {
    import ReductionResult._
    var latestState = state
    var allResults = Set[ReductionResult]()
    var resultsFromPass = Set[ReductionResult](Reduced) // not accurate (no pass yet), just initial condition to enter loop...
    while (!latestState.isBound && !resultsFromPass.contains(ReducedToEmpty) && resultsFromPass.contains(Reduced)) {
      resultsFromPass = Set()
      val reductions = system.exprs.flatMap(_.generateReductions(latestState))
      val reductionIterator = reductions.iterator
      while (reductionIterator.hasNext && !resultsFromPass.contains(ReducedToEmpty)) {
        val reduction = reductionIterator.next()
        val (reducedState, reductionResult) = reduction.reduce(latestState)
        resultsFromPass = resultsFromPass + reductionResult
        latestState = reducedState
      }
      allResults = allResults ++ resultsFromPass
    }
    (latestState, allResults)
  }

  def initialSearchState(system: SymbolicSystem): Seq[SearchNode] = {
    require(system.exprs.size > 0)
    val initialState = new DomainStore(system)

    val artificialExprToRestrict: Expr = system.exprs.head
    // artificial root of search, if we backtrack over this artificial decision, the whole system is recognized as infeasible
    val artificialChoice = RestrictToReduction(artificialExprToRestrict, artificialExprToRestrict.initialDomain)

    val (reducedState, _) = artificialChoice.reduce(initialState)
    val (propagatedState, _) = propagate(reducedState, system)

    val initialSearchNode = SearchNode(initialState, artificialChoice, false, propagatedState)
    Seq(initialSearchNode)
  }

  // backtrack to feasible space if necessary
  // take a single forward search step (from the current state if feasible, or else from the backtracked state)
  def step(searchState: Seq[SearchNode], reductionChooser: ReductionChooser, system: SymbolicSystem): Seq[SearchNode] = {
    if (searchState.isEmpty) {
      // if the initial state requires search to discover it's infeasible, we won't have an infeasible DomainStore to examine
      // we will have to pop off the initial state in order to signify that the intitial state is infeasible
      throw new IllegalArgumentException("empty search state is defined to be the result from backtracking, problem is infeasible, cannot step()")
    }
    if (searchState.head.nextState.isBound) {
      throw new IllegalArgumentException("latest SearchNode is bound, we have a solution, cannot step()")
    }
    if (searchState.head.nextState.isInfeasible) { // need to backtrack, and leave a new SearchNode as the latest
      var result = searchState
      while (result.nonEmpty && result.head.triedOpposite) {
        result = result.tail // pop
      }
      if (result.nonEmpty) { // else it's empty, which is the defined terminal search state for an infeasible system
        val SearchNode(prevState, choice, _, _) = result.head
        result = result.tail
        val oppositeChoice = choice.opposite
        val (oppositeReducedState, _) = oppositeChoice.reduce(prevState)
        val (oppositePropagatedState, _) = propagate(oppositeReducedState, system)
        val oppositeSearchNode = SearchNode(prevState, oppositeChoice, true, oppositePropagatedState)
        result = oppositeSearchNode +: result
      }
      result
    } else { // should step forward, and add one new SearchNode as latest
      val prevState = searchState.head.nextState
      val choice = reductionChooser.chooseReduction(prevState, system)
      val (reducedState, _) = choice.reduce(prevState)
      val (propagatedState, _) = propagate(reducedState, system)
      val searchNode = SearchNode(prevState, choice, false, propagatedState)
      searchNode +: searchState
    }
  }

}
