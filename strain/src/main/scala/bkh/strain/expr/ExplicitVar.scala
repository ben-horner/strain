package bkh.strain.expr

import bkh.strain.domain.ExplicitDomain
import bkh.strain.system.{Domain, DomainReduction, DomainStore, Expr, ReductionResult, RestrictToReduction}

object ExplicitVar {
  def apply[A](domainValues: Set[A]): ExplicitVar[A] = {
    new ExplicitVar[A](new ExplicitDomain[A](domainValues))
  }
}

case class ExplicitVar[A](override val initialDomain: ExplicitDomain[A], id: Long = Expr.nextId()) extends Expr {
  override def subExprs: Seq[Expr] = Seq()

  override def domain(withinState: DomainStore): ExplicitDomain[A] =
    withinState.domain(id).asInstanceOf[ExplicitDomain[A]]

  override def computeDomain(withinState: DomainStore): ExplicitDomain[A] = domain(withinState)

  override def generateReductions(fromState: DomainStore): Seq[DomainReduction] = Seq()

  override def remove(remove: Domain, fromState: DomainStore): (DomainStore, ReductionResult) = {
    val (newDomain, reductionResult) = domain(fromState).remove(remove.asInstanceOf[ExplicitDomain[A]])
    (DomainStore(fromState.idToDomain + (id -> newDomain)), reductionResult)
  }

  override def restrictTo(restrictTo: Domain, fromState: DomainStore): (DomainStore, ReductionResult) = {
    val (newDomain, reductionResult) = domain(fromState).restrictTo(restrictTo.asInstanceOf[ExplicitDomain[A]])
    (DomainStore(fromState.idToDomain + (id -> newDomain)), reductionResult)
  }

  override def domainBisectionReduction(fromState: DomainStore): DomainReduction = {
    RestrictToReduction(this, domain(fromState).bisect())
  }
}