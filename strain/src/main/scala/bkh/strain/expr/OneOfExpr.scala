package bkh.strain.expr

import bkh.strain.domain.ExplicitDomain
import bkh.strain.system.{Domain, DomainReduction, DomainStore, Expr, ReductionResult, RestrictToReduction}

case class OneOfExpr[A](subExprs: Seq[ExplicitVar[A]],
                        initialDomain: ExplicitDomain[A],
                        id: Long = Expr.nextId()) extends Expr {
  override def domain(withinState: DomainStore): ExplicitDomain[A] =
    withinState.domain(this.id).asInstanceOf[ExplicitDomain[A]]

  override def computeDomain(withinState: DomainStore): ExplicitDomain[A] = {
    val subExprUnion = subExprs.map(_.domain(withinState).values).reduce(_ ++ _)
    val currentDomain = domain(withinState).values
    ExplicitDomain[A](currentDomain.intersect(subExprUnion))
  }

  override def generateReductions(fromState: DomainStore): Seq[DomainReduction] = {
    val computedDomain = computeDomain(fromState)
    val reductionForThis = if ((domain(fromState).values -- computedDomain.values).nonEmpty) {
      Seq(RestrictToReduction(this, computedDomain))
    } else {
      Seq()
    }
    // There's the implicit domain of which subExpr this OneOfExpr gets bound to
    // We can only reduce a subExpr domain if that implicit parameter is bound
    val potentials = subExprs.filter(expr => expr.domain(fromState).values.intersect(computedDomain.values).nonEmpty)
    val subExprReductions = if (potentials.size == 1) {
      Seq(RestrictToReduction(potentials.head, computedDomain))
    } else {
      Seq()
    }
    reductionForThis ++ subExprReductions
  }

  override def remove(remove: Domain, fromState: DomainStore): (DomainStore, ReductionResult) = {
    val toRemove = remove.asInstanceOf[ExplicitDomain[A]]
    val (newDomain, reductionResult) = domain(fromState).remove(toRemove)
    (DomainStore(fromState.idToDomain + (id -> newDomain)), reductionResult)
  }

  override def restrictTo(restrictTo: Domain, fromState: DomainStore): (DomainStore, ReductionResult) = {
    val toRestrictTo = restrictTo.asInstanceOf[ExplicitDomain[A]]
    val (newDomain, reductionResult) = domain(fromState).restrictTo(toRestrictTo)
    (DomainStore(fromState.idToDomain + (id -> newDomain)), reductionResult)
  }

  override def domainBisectionReduction(fromState: DomainStore): DomainReduction = {
    RestrictToReduction(this, domain(fromState).bisect())
  }
}
