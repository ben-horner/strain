package bkh.strain.expr

import bkh.strain.domain.ExplicitDomain
import bkh.strain.system.{Domain, DomainReduction, DomainStore, Expr, ReductionResult, RemoveReduction, RestrictToReduction}

object AllDifferentExpr {
  def apply[A](subExprs: Seq[ExplicitVar[A]], initialValues: Set[Boolean]): AllDifferentExpr[A] = {
    new AllDifferentExpr[A](subExprs, initialValues)
  }
}

case class AllDifferentExpr[A](subExprs: Seq[ExplicitVar[A]],
                               initialDomain: ExplicitDomain[Boolean],
                               id: Long = Expr.nextId()) extends Expr {
  def this(subExprs: Seq[ExplicitVar[A]], initialValues: Set[Boolean]) =
    this(subExprs, ExplicitDomain[Boolean](initialValues))

  override def domain(withinState: DomainStore): ExplicitDomain[Boolean] =
    withinState.domain(id).asInstanceOf[ExplicitDomain[Boolean]]

  override def computeDomain(withinState: DomainStore): ExplicitDomain[Boolean] = {
    val domains = subExprs.map(_.domain(withinState).values)
    val canBeTrue = domains.reduce(_ ++ _).size >= subExprs.size
    val canBeFalse = { // at least two variables share a value
      domains.combinations(2).exists { case Seq(a, b) => a.intersect(b).nonEmpty }
    }
    var canBeSet = Set[Boolean]()
    if (canBeTrue) canBeSet = canBeSet + true
    if (canBeFalse) canBeSet = canBeSet + false

    val currentDomain = domain(withinState).values
    ExplicitDomain[Boolean](currentDomain.intersect(canBeSet))
  }

  override def generateReductions(fromState: DomainStore): Seq[DomainReduction] = {
    val computedDomain = computeDomain(fromState)
    val reductionForThis: Seq[DomainReduction] = if ((domain(fromState).values -- computedDomain.values).nonEmpty) {
      Seq(RestrictToReduction(this, computedDomain))
    } else {
      Seq()
    }
    import ExplicitDomain.{FalseOnly, TrueOnly}
    val subExprReductions: Seq[DomainReduction] =
      if (computedDomain == TrueOnly || computedDomain == FalseOnly) {
        val exprToDomain = subExprs.map(se => se -> se.domain(fromState)).toMap
        if (computedDomain == TrueOnly) {
          // values from bound domains should be removed from unbound domains
          // additional work could be done, if there is a subset of subExprs which cover a set of the same number of values, then no other expression can have any of those values
          val (bound, unbound) = exprToDomain.partition(_._2.isBound)
          val boundValues = bound.values.map(_.values).reduce(_ ++ _)
          unbound.filter(_._2.values.intersect(boundValues).nonEmpty).map { case (expr, domain) =>
            RemoveReduction(expr, ExplicitDomain[A](boundValues))
          }.toSeq
        } else {
          // if (computedDomain == FalseOnly)
          //   could only reduce if all but one is bound, could restrict unbound variable to the set of other bound values
          Seq()
        }
      } else {
        Seq()
      }
    reductionForThis ++ subExprReductions
  }

  override def domainBisectionReduction(fromState: DomainStore): DomainReduction = {
    RestrictToReduction(this, domain(fromState).bisect())
  }

  override def remove(remove: Domain, fromState: DomainStore): (DomainStore, ReductionResult) = {
    val toRemove = remove.asInstanceOf[ExplicitDomain[Boolean]]
    val (newDomain, reductionResult) = domain(fromState).remove(toRemove)
    (DomainStore(fromState.idToDomain + (id -> newDomain)), reductionResult)
  }

  override def restrictTo(restrictTo: Domain, fromState: DomainStore): (DomainStore, ReductionResult) = {
    val toRestrictTo = restrictTo.asInstanceOf[ExplicitDomain[Boolean]]
    val (newDomain, reductionResult) = domain(fromState).restrictTo(toRestrictTo)
    (DomainStore(fromState.idToDomain + (id -> newDomain)), reductionResult)
  }

}