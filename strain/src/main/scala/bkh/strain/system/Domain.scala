package bkh.strain.system

// the Domain of an Expr is the set of values it's allowed to take on
// propagation of constraints will reduce Domains as discovery / search proceeds
trait Domain {
  def isEmpty: Boolean

  def isBound: Boolean
}
