package bkh.strain.system

object SymbolicSystem {
  def apply(): SymbolicSystem = new SymbolicSystem()

  def apply(idToExpr: Map[Long, Expr]): SymbolicSystem = new SymbolicSystem(idToExpr)

  def apply(exprs: Seq[Expr]): SymbolicSystem = new SymbolicSystem(exprs)
}

// the set of Exprs that defines the system
// the point is to find a solution which binds each Expr to a single value from it's domain
// possibly the point is to optimize some objective Expr as well (either minimize or maximize)
case class SymbolicSystem(idToExpr: Map[Long, Expr], nameToId: Map[String, Long]) {
  def this() = this(Map(), Map())

  def this(idToExpr: Map[Long, Expr]) = this(idToExpr, Map())

  def this(exprs: Seq[Expr]) = this(exprs.map(expr => (expr.id -> expr)).toMap)

  // TODO: superExprs doesn't need to be a constructor param -- a real case class member because it's completely determined by another class member... right?
  lazy val superExprs: Map[Expr, Set[Expr]] = {
    idToExpr.values.flatMap { sup =>
      sup.subExprs.map(sub => (sub, sup)) // this will produce the edige list of all sub -> sup edges
    }.groupBy(_._1) // this will turn the edge list into an adjacency list: sub -> Iterable[(sub, sup)]
      .map{ case (sub, subToSups) => (sub -> subToSups.map(_._2).toSet) }
  }

  def exprs: Seq[Expr] = idToExpr.values.toSeq

  def exprWithId(id: Long): Expr = idToExpr(id)

  def exprWithName(name: String): Expr = exprWithId(nameToId(name))

  def contains(expr: Expr): Boolean = idToExpr.contains(expr.id)

  def subExprsContained(expr: Expr): Boolean = {
    contains(expr) && expr.subExprs.forall(subExprsContained)
  }

  def selfContained(): Boolean = {
    idToExpr.values.forall(subExprsContained)
  }

  // do we need to worry about subExprs leading to cycles (infinite recursion -- is that impossible to construct immutably)?
  def add(expr: Expr): SymbolicSystem = {
    val withSubExprsAdded = expr.subExprs
      .filterNot(contains)
      .foldLeft(this)(_.add(_))
    SymbolicSystem(withSubExprsAdded.idToExpr + (expr.id -> expr), withSubExprsAdded.nameToId)
  }

  def add(expr: Expr, name: String): SymbolicSystem = {
    add(expr).nameExpr(expr, name)
  }

  def nameExpr(expr: Expr, name: String): SymbolicSystem = SymbolicSystem(idToExpr, nameToId + (name -> expr.id))
}