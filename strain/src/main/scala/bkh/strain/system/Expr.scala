package bkh.strain.system


// an Expr is a core component of the constrained system
// variables with explicit domains will be expressions
// higher level expressions combine variables and other subExprs
// Exprs know how to compute their effective Domain based on the Domains of their subExprs
// Exprs know how to propagate reductions in their own Domains to the Domains of their subExprs
// an Exprs has an id which allows it to find the instance of it's Domain within a particular DomainState
object Expr {
  private var _nextId: Long = 0
  def nextId(): Long = {
    val result = _nextId;
    _nextId = _nextId + 1
    require(_nextId > result)
    result
  }
}

abstract class Expr() {
  val id: Long
  def subExprs: Seq[Expr] // TODO: should this be a set rather than a Seq?
  def initialDomain: Domain

  def domain(withinState: DomainStore): Domain // lookup withinState
  def computeDomain(withinState: DomainStore): Domain // computed from subExprs withinState
  def generateReductions(fromState: DomainStore): Seq[DomainReduction] // generate reductions which can be executed later (callbacks) -- try not to generate reductions which have no effect

  def domainBisectionReduction(fromState: DomainStore): DomainReduction // TODO: let caller choose top vs bottom half for ordered domains? -- domain knowledge could be useful

  def remove(remove: Domain, fromState: DomainStore): (DomainStore, ReductionResult)
  def restrictTo(restrictTo: Domain, fromState: DomainStore): (DomainStore, ReductionResult)
}
