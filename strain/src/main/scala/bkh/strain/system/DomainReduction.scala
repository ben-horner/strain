package bkh.strain.system

// an instruction to reduce the domain of some Expr
// this is reified so that the reduction can be delayed (so many reductions can be collected and executed "at once")
sealed trait DomainReduction {
  def exprToReduce: Expr
  def opposite: DomainReduction // for when a reduction is discovered to be infeasible
  def reduce(state: DomainStore): (DomainStore, ReductionResult)
}

case class RemoveReduction(exprToReduce: Expr, remove: Domain) extends DomainReduction {
  override def opposite: DomainReduction = RestrictToReduction(exprToReduce, remove)
  override def reduce(state: DomainStore): (DomainStore, ReductionResult) = exprToReduce.remove(remove, state)
}

case class RestrictToReduction(exprToReduce: Expr, restrictTo: Domain) extends DomainReduction {
  override def opposite: DomainReduction = RemoveReduction(exprToReduce, restrictTo)
  override def reduce(state: DomainStore): (DomainStore, ReductionResult) = exprToReduce.restrictTo(restrictTo, state)
}
