package bkh.strain.system

sealed trait ReductionResult

case object ReductionResult {
  case object NoReduction extends ReductionResult
  case object ReducedToEmpty extends ReductionResult
  case object ReducedToBound extends ReductionResult
  case object Reduced extends ReductionResult // not Empty, not Bound
}
