package bkh.strain.system

object DomainStore {
  def apply(): DomainStore = new DomainStore()

  def apply(symbolicSystem: SymbolicSystem): DomainStore = new DomainStore(symbolicSystem)
}

// the set of actual instances of the Domains of a system at a particular point during a search for a solution
// the DomainState references Domains by ids, so that they can be looked up for particular Exprs
case class DomainStore(idToDomain: Map[Long, Domain]) {
  def this() = this(Map[Long, Domain]())

  def this(system: SymbolicSystem) = this(
    system.exprs.foldLeft(Map[Long, Domain]()) { (idToDomain, expr) =>
      idToDomain + (expr.id -> expr.initialDomain)
    })

  lazy val isInfeasible: Boolean = idToDomain.values.exists(_.isEmpty)
  lazy val isBound: Boolean = idToDomain.values.forall(_.isBound)

  def domain(id: Long): Domain = idToDomain(id)
  //  def register(expr: Expr, domain: Domain): DomainStore = {
  //    DomainStore(idToDomain + (expr.id -> domain))
  //  }
}