package bkh.strain.system

import bkh.strain.expr.ExplicitVar

import scala.util.Random

trait ReductionChooser {
  def chooseReduction(state: DomainStore, system: SymbolicSystem): DomainReduction
}

object ReductionChooser {
  val rng: Random = new Random(42)

  val SmallestBisectingChooser = WeightedRankChooser(domainSizeWeight=1.0, connectivityWeight=0.0)
  val MostConnectedBisectingChooser = WeightedRankChooser( domainSizeWeight=0.0, connectivityWeight= -1.0)
}

case object RandomBisectingChooser extends ReductionChooser {
  override def chooseReduction(state: DomainStore, system: SymbolicSystem): DomainReduction = {
    val unboundExprs = system.exprs.filter(!_.domain(state).isBound)
    import ReductionChooser.rng
    val chosenExpr = unboundExprs(rng.nextInt(unboundExprs.size))
    chosenExpr.domainBisectionReduction(state)
  }
}

// given a weighting of importance of domain size rank versus connectivity rank
// choose expr with minimum total weight and bisect it
// choosing Exprs with smaller domains first binds variables faster
// choosing more connected Exprs first propagates more reductions
// TODO: when we have a new primitive expression (other than ExplicitVar), this should be changed (add Domain.bisectionsUntilBound()?)
case class WeightedRankChooser(domainSizeWeight: Double = 1.0, connectivityWeight: Double = -1.0) extends ReductionChooser {
  override def chooseReduction(state: DomainStore, system: SymbolicSystem): DomainReduction = {
    val unboundExplicitVars: Seq[ExplicitVar[_]] = system.exprs
      .filter(_.isInstanceOf[ExplicitVar[_]])
      .map(_.asInstanceOf[ExplicitVar[_]])
      .filter(!_.domain(state).isBound)

    val groupedBySize: Map[Int, Seq[ExplicitVar[_]]] = unboundExplicitVars.groupBy(ev => ev.domain(state).values.size)
    val sizeToRank: Map[Int, Int] = groupedBySize.keySet.toSeq.sorted.zipWithIndex.toMap
    val varToSizeRank: Map[ExplicitVar[_], Int] = groupedBySize.flatMap{ case (size, evs) => evs.map(ev => (ev -> sizeToRank(size)) ) }

    val groupedByConnections: Map[Int, Seq[ExplicitVar[_]]] = unboundExplicitVars.groupBy(ev => system.superExprs(ev).size + ev.subExprs.size)
    val connectionsToRank: Map[Int, Int] = groupedByConnections.keySet.toSeq.sorted.zipWithIndex.toMap
    val varToConnectivityRank: Map[ExplicitVar[_], Int] = groupedByConnections.flatMap{ case (connections, evs) => evs.map(ev => (ev -> connectionsToRank(connections)) ) }

    val varToWeight: Map[ExplicitVar[_], Double] = unboundExplicitVars.map( ev => (ev  -> (domainSizeWeight * varToSizeRank(ev) + connectivityWeight * varToConnectivityRank(ev))) ).toMap
    val minWeight: Double = varToWeight.values.min
    val minWeightVars: Seq[ExplicitVar[_]] = varToWeight.toSeq.filter(_._2 == minWeight).map(_._1)
    import ReductionChooser.rng
    val chosenExpr = minWeightVars(rng.nextInt(minWeightVars.size))
    chosenExpr.domainBisectionReduction(state)
  }
}
