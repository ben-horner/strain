package bkh.strain.domain

import bkh.strain.system.{Domain, ReductionResult}

object ExplicitDomain {
  val FalseOnly = ExplicitDomain[Boolean](Set(false))
  val TrueOnly = ExplicitDomain[Boolean](Set(true))
  val AllBools = ExplicitDomain[Boolean](Set(false, true))

  def reductionResult[A](oldDomain: Set[A], newDomain: Set[A]): ReductionResult = {
    if (oldDomain == newDomain) ReductionResult.NoReduction
    else if (newDomain.isEmpty) ReductionResult.ReducedToEmpty
    else if (newDomain.size == 1) ReductionResult.ReducedToBound
    else ReductionResult.Reduced
  }
}

case class ExplicitDomain[A](values: Set[A]) extends Domain {
  override def isEmpty: Boolean = values.isEmpty

  override def isBound: Boolean = values.size == 1

  def remove(that: ExplicitDomain[A]): (ExplicitDomain[A], ReductionResult) = {
    val newValues = values -- that.values
    val reductionResult = ExplicitDomain.reductionResult(values, newValues)
    (ExplicitDomain[A](newValues), reductionResult)
  }

  def restrictTo(that: ExplicitDomain[A]): (ExplicitDomain[A], ReductionResult) = {
    val newValues = values.intersect(that.values)
    val reductionResult = ExplicitDomain.reductionResult(values, newValues)
    (ExplicitDomain[A](newValues), reductionResult)
  }

  def bisect(): ExplicitDomain[A] = ExplicitDomain[A](values.take((values.size + 1) / 2))
}